﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
	[SerializeField] float mainThrust = 1000f;
	[SerializeField] float rotationThrust = 100f;
	[SerializeField] AudioClip mainEngine;

	[SerializeField] ParticleSystem mainEngineParticles;
	[SerializeField] ParticleSystem leftThrusterParticles;
	[SerializeField] ParticleSystem rightThrusterParticles;

	Rigidbody rigidBody;
	AudioSource audioSource;

	bool isAlive;

	// Start is called before the first frame update
	void Start()
	{
		rigidBody = GetComponent<Rigidbody>();
		audioSource = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update()
	{
		ProcessThrust();
		ProcessRotation();
	}

	void ProcessThrust()
	{
		if (Input.GetKey(KeyCode.Space))
		{
			StartThrusting();
		}
		else
		{
			StopThrusting();
		}
	}

	void StartThrusting()
	{
		// Vector3.up shorthand for (0,1,0).
		rigidBody.AddRelativeForce(Vector3.up * mainThrust * Time.deltaTime);
		if (!audioSource.isPlaying)
		{
			audioSource.PlayOneShot(mainEngine);
		}
		if (!mainEngineParticles.isPlaying)
		{
			mainEngineParticles.Play();
		}
	}
	private void StopThrusting()
	{
		audioSource.Stop();
		mainEngineParticles.Stop();
	}

	void ProcessRotation()
	{
		if (Input.GetKey(KeyCode.A))
		{
			RotateLeft();
		}
		else if (Input.GetKey(KeyCode.D))
		{
			RotateRight();
		}
		else
		{
			StopRotating();
		}
	}

	// Handle the left rotation
	private void RotateLeft()
	{
		// Vector3.forward shorthand for (0,0,1).
		ApplyRotation(rotationThrust);
		if (!rightThrusterParticles.isPlaying)
		{
			rightThrusterParticles.Play();
		}
	}

	// Handle the right rotation
	private void RotateRight()
	{
		// Vector3 shorthand for (0,0,-1).
		ApplyRotation(-rotationThrust);
		if (!leftThrusterParticles.isPlaying)
		{
			leftThrusterParticles.Play();
		}
	}

	// Handle stop the rotation
	private void StopRotating()
	{
		rightThrusterParticles.Stop();
		leftThrusterParticles.Stop();
	}

	void ApplyRotation(float rotationThisFrame)
	{
		// Freezing rotation so we can manually rotate.
		rigidBody.freezeRotation = true;
		transform.Rotate(Vector3.forward * rotationThisFrame * Time.deltaTime);
		// Unfreezing rotation so the physics system can take over.
		rigidBody.freezeRotation = false;
	}
}
