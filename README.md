# Space Game



## Getting started

 ```
cd existing_repo
git remote add origin https://gitlab.com/jakie.doan/space-game.git
git branch -M main
git push -uf origin main
```

## Name
Space Game

## Description
Space Game.

## Run .exe
Open Build folder to run the game .exe file.

# Demo
![image](https://gitlab.com/jakie.doan/space-game/-/raw/main/Demo/Demo.gif)
